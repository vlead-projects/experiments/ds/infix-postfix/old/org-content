#+TITLE: Infix to Postfix Conversion and Evaluation of Postfix Expressions Experiment
#+AUTHOR: Sowrya Deep, Keshavan
#+DATE: [2018-05-16 Wed]
#+TAGS: pmbl lu task txta
#+EXCLUDE_TAGS:
#+OPTIONS: ^:nil' prop:t
#+SETUPFILE: ../org-templates/level-1.org
* Infix to Postfix Conversion and Evaluation of Postfix Expressions    :title:
:PROPERTIES:
:SCAT: title
:CUSTOM_ID: exp-title-cnt
:END:



* Preamble                                                             :pmbl:
:PROPERTIES:
:SCAT: pmbl
:CUSTOM_ID: infiix-to-postfix-experiment
:END:
This is an experiment to explain Infix to Postfix conversion of expressions and 
evaluation of Postfix expression.

** Experiment Structure                                                :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Infix-Postfix-Conversion-and -Evaluation-exp-structure
:END:      

*** Structure of Experiment                                            :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: Image
:CUSTOM_ID: Infix-Postfix-Conversion-and -Evaluation-exp-structure-req
:END:      
	An image which shows the structure of the experiment.

** Unit Weightages                                                     :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Infix-and-Postfix-unit-weightages
:END:      

*** Weightage of Each Module                                           :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: exp-weightage
:END:      
	
        | Module                                      | Weightage | Expectation                                                |
        | Pre-test                                    |       5% | Solve All Questions                                        |
        | Formally defining the Expressions           |       5% | Become able to indentify different expressions             |
        | Introduction to Infix to Postfix Conversion |       10% | Able to Convert Infix Expression without using stack       |
        | Conversion using Stack Operations           |       25% | Try all operations with atleast two examples using stack   |
        | Evaluation of the Postfix Expressions       |       25% | Able to evaluate any postfix expression(solve atleast two) |
        | Post-assessment                             |       30% | Solve All Questions                                        |



** Learning Objectives                                                 :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Infix-to-Postfix-obj-task
:END:

*** Infix and Postfix conversion and Evaluaiton Learning Objectives                                    :txta:
:PROPERTIES:
:SCAT: text
:CUSTOM_ID: learning-obj
:END:

    In this experiment on linked lists you will learn the following topics:
    + Formal Definations of Infix and Postfix expressions.
    + Basic concepts behind Infix to Postfix Conversion.  
    + Conversion methods from Infix to Postfix. 
    + Evaluation method of Postfix Expressions.


** Prerequisite                                                        :task:
:PROPERTIES:
:SCAT: scene
:CUSTOM_ID: Infix-to-Postfix-prerequisites-tsk
:END:

*** Pre-requisites for Infix to Postfix Conversion and Evaluation       :reqa:
:PROPERTIES:
:SCAT: reqa
:TYPE: InteractiveJS
:CUSTOM_ID: Infix-to-Postfix-prereq-req
:END:

An artefact that interactively shows pre-requisites required
and lets the user go to links that'll help user learn them.

** Pre-test                                                            :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Infix-to-postfix-pretest
:END:
A simple test that illustrates the prerequisites that the
student must have in order to attempt this unit.

*** Questions                                                          :reqa:
:PROPERTIES:
:SCAT: reqa
:CUSTOM_ID: Infix-and-Postfix-pretest-questions
:END:
Come up with a set of questions that test knowledge of the
student on concepts that Singly Linked Lists depends on.
The concepts that this quiz should cover is:
   
# Fill here
# like regular algebraic expressions,Push and Pop operations of stack.


* Defining the different expression and the required conversion        :lu: 
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: defining-all-illustrate-lu
:END:  

** Illustrate the idenfications of the expressions and their respective conversions :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: expression-identification-illustrate-task
:END:

*** Identification of Postfix                                             :txta: 
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: Postfix-identification
:END:
When an expression is given with the last element being an 
operator is on way to identify a postfix expression.

*** Show the Postfix expressions 	                                        :reqa:
:PROPERTIES:
:CUSTOM_ID: Postfix-expressions-pics
:TYPE: Image
:SCAT: reqa
:END:
Shows an example of a Postfix expression to understand more clearly after 
the text definition given before this artifact.


*** Show the Postfix expresssion excercise                                  :reqa:
:PROPERTIES:
:CUSTOM_ID: postfix-expression-show
:SCAT: reqa
:REFERENCE_ID: Postfix-expression-pics
:TYPE: InteractiveJS
:END:
show images of expression and the user has to identify whether the particular
expression is a postfix expression among a mixture of other expressions.

Provide some example images of Postfix expressions.
     

*** Identification of Infix 					       :txta: 
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: Infix-Identification
:END:
When an expression has an operator between every two numbers
then it can be said to be an Infix Expression,is one way to 
identify an Infix Expression.

*** Show the Infix expresssion excercise 					       :reqa:
:PROPERTIES:
:CUSTOM_ID: Infix-identification-show
:SCAT: reqa
:REFERENCE_ID: Infix-indentification
:TYPE: InteractiveJS
:END:      
show images of expressions(infix,postfix and prefix  expressions) and the user 
has to identify whether the particular expression is an Infix expression 
among a mixture of other expressions.



*** Show the Postfix Expression for an Infix Expression                :reqa:
:PROPERTIES:
:CUSTOM_ID: Infix-Conversion-show
:SCAT: reqa
:TYPE: Image
:END:      
Provide an example image of Infix expression to Postfix Expression Conversion


** Find Postfix and Infix Expressions 				       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Identify-Epression-task
:END:      


*** Identification of Infix and Postfix expressions 		       :reqa:
:PROPERTIES:
:CUSTOM_ID: Idetification-of-Expressions-interactive
:SCAT: reqa
:TYPE: InteractiveJS
:END: 
give a test for the learner to attempt on identifying the Infix and Postfix 
Expressions among a clutter of images with both in them.



* Infix to Postfix Conversion Introduction Unit 		       :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: conversion-unit
:END:      

** Conversion Introduction                                             :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: conversion-intro
:END:      

*** Conversion involving these following operations                    :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: conversion-steps
:END:      

# Defining and Usage Part- defining infix,postfix and the conversion of infix to postix and its usage,

Reading the expression

Stack Operations - Rules pertaining to the use and functioning 
of stack in the conversion process that is push and pop operations 
and the rules that are to be followed to attain the desired conversion. 

Filling out the output expression stack and it's maintenance.


*** Picture of the infix and output expressions and conversion 	       :reqa:
:PROPERTIES:
:CUSTOM_ID: conversion-intro-pic
:TYPE: Image
:SCAT: reqa
:END:

[[https://olimex.files.wordpress.com/2013/07/in-post.gif?w=403]]

 # [[http://lh4.ggpht.com/-eXEecmwh39U/UKk0mt70OaI/AAAAAAAAB3M/sq7QuXFQ4_s/clip_image001_thumb%25255B1%25255D.gif?imgmax=800]]

Design the image of an Inifx expression and a Postfix expression 
and the flow chart of their Conversion from infix to postfix.


*** Explaining the conversion as shown in the image artifact          :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: conversion-explanation
:END:      

Explanation regarding the simple conversion shown in the image artifact 
which is done without thinking about the stack


* Conversion using Stack Operations 				       :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: Conversion-illustrate-lu
:END:

** Conversion of Infix to Postfix using Stack Operations               :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: Stack-Operations-task
:END:      
Push Operations -

Pop Operations -

*** Scenarios with Push Operations on Stack while Conversion          :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: Push-Operations-show
:END:
Explain the rules for the Push Operations from the algorithm for the conversion.

*** Demonstrate the Push Operations 				       :reqa:
:PROPERTIES:
:CUSTOM_ID: Push-Operations-Demostrations
:SCAT: reqa
:TYPE: Image
:END:
Show images demonstrating push operations in different scenarios.


*** Scenarios with Pop Operations on Stack while Conversion           :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: Pop-Operations-task
:END:
Explain the rules for the Pop Operations from the algorithm for the conversion.

*** Demonstrate the Pop Operations 				       :reqa:
:PROPERTIES:
:CUSTOM_ID: Pop-Operations-Demostrations
:SCAT: reqa
:TYPE: Image
:END:
Show images demonstrating pop operations in different scenarios


** Infix to Postfix Conversion Exercise                               :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: conversion-exercise-task
:END: 
An exercise for Postfix Evaluation.

*** Perform the conversion                                            :reqa:
:PROPERTIES:
:CUSTOM_ID: conversion-interactive
:SCAT: reqa
:REFERENCE_ID: none
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs the conversion
of an infix expression to postfix expression using stack operations.

The required expression to convert into Postfix is taken and a pointer starting 
from the left end side of the expression shifts one position to the right after 
either (push or pop) operation of the operator in the present position or if it
is an operand then push it directly to the ouput stack.

The user selects a button among 
a Push button - where, on clicking it the element the pointer is at get pushed 
into the stack and be the top element in the stack.
a Pop button - where, on clicking it the element at the top of the stack gets 
popped up from the stack and goes to the 
an Ignore the element on top of stack - This ignores(removes) the element that 
is currently on top of the stack.
an Ignore the element that the pointer is at - This ignores(removes) the element that 
is currently pointed by the pointer.


* Evaluation of Postfix Unit 					       :lu:
:PROPERTIES:
:SCAT: lu
:CUSTOM_ID: postfix-evaluation-unit
:END:

** Evaluation Introduction 					       :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: evaluation-intro
:END:
Giving and idea of what is Evaluation of Postfix.

*** Evaluation involving these following operations 		       :txta:
:PROPERTIES:
:SCAT: txta
:CUSTOM_ID: evaluation-steps
:END: 
Evaluation - explaining the method of evaluating the postfix expression


*** Demonstration of Postfix Evaluation                                :reqa:
:PROPERTIES:
:CUSTOM_ID: postfix-evaluation-pic
:SCAT: reqa
:TYPE: Image
:END:
The image demonstrates how to evaluate postfix expression using stack.

** Postfix Expression Evaluation Exercise                              :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: postfix-evaluation
:TYPE: InteractiveJS
:END:
The student interactively performs 
the evaluation of a postfix expression.

*** Perform the evaluation of Postfix Expressions                     :reqa:
:PROPERTIES:
:CUSTOM_ID: evaluation-interactive
:SCAT: reqa
:REFERENCE_ID: none
:TYPE: InteractiveJS
:END:
Provide an artifact where a student interactively performs the evaluation 
of a postfix expression. 


* Post Assessment Unit                                                 :lu:
:PROPERTIES: 
:SCAT: lu
:CUSTOM_ID: post-assessment-lu
:END:      
In this unit, we'll have a quiz based on all the concepts
learned in this module.

** Post-Quiz                                                           :task:
:PROPERTIES:
:SCAT: task
:CUSTOM_ID: post-assessment-task
:END:
Answer all the questions to test the understanding of your
concepts learnt in this module.

*** Questions                                                          :reqa:
:PROPERTIES:
:SCAT: reqa
:CUSTOM_ID: post-assessment-questions
:END:
Have a quiz that has a set of questions that test the
concepts learnt in the Infix and Postfix Expression,their Conversion and the 
Evaluation of Postfix Expressions.


* Final Performance                                                    :lu:
:PROPERTIES: 
:SCAT: lu
:CUSTOM_ID: final-performance
:END:      

** Result							       :task:
:PROPERTIES: 
:SCAT: lu
:CUSTOM_ID: final-exp-performance
:END:      
Show final performance of whole experiment.


* Further Studies                                                      :lu:
:PROPERTIES: 
:SCAT: lu
:CUSTOM_ID: final-references-exp-list
:END:      

Give links to more resources on linked studies 

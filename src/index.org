#+TITLE: Story
#+AUTHOR: <Keshavan Seshadri> <Sowrya Deep>
#+DATE: [2017-12-17 Thu]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  + Books don't always explain concepts in the best possible way.
  + A project done with determination and sincerity to effectively help the learner understand concepts with no prerequisite.

* Scope of the project
  + This repository contains all the codes,resources and documentation for the infix-to-postfix experiment.
  + We want to build a project that would help students learn and understand infix-to-postfix and postfix evaluation visually through explanations,examples,exercises and self tests.
  + For realising this pedagogy would be followed.
  + Can help millions of students understand the concept of infix to postfix and postfix evaluation through visual illustrative explanations and an effective practice and feedback mechanism.
* Motivation
  + We want to build a teaching aid that can help students across the globe understand fundamental concepts in a better wa1y.
  + We want students to enjoy the process of learning and be able to apply and recall better.
* Members of the Project


| *Name*              | Keshavan Seshadri | Sowrya Deep              |

| Year of Study  | 1st year Btech IIIT-H | 2nd year Btech IIIT-H |

| Email-ID          | keshavanseshadri@gmail.com | sowrya1682@gmail.com       |

| Phone Number      | 7010615105   | 7989025718   |

| gitlab handles | K7S3 | Sowrya |

| github handles | K7S3 | natsuset |

* Running Status 

| SN0. | Purpose | Start Date | End Date | Status | Remarks |

| 1. | [[./exp-cnt/concrete.org][Creating Experiment Structure for infix-to-postfix- A1 phase]] | 14th May, 2018 | 20th May, 2018 | Finished | Closed |
| 2. | [[https://gitlab.com/vlead-projects/experiments/linkedlists/linkedlist/milestones/2][Developing a well structured story-board - A2]] | 21st May, 2018 | 25th May, 2018 | Needs to be changed according to new format | Running | 


* Assumptions 

  
 
* Documents
  | SNo. | Purpose              | Link        |
  |    1 | Requirements         | [[./requirements][req]]         |
  |    2 | Realization Plan     | [[./realization-plan][rlz-plan]]    |
  |    3 | Experiment Structure | [[./exp-cnt][exp-plan]]    |
  |    4 | Story Board          | [[./story-board][story-board]] |
  |    5 | Experiences          | [[./docs/experiences.org][experiences]]            |
* Assumptions 
  + The learner just needs to know the basic mathematics.
* Stake-holders
  + College 1st year students who don't know to google properly - Just Joking(it's for me too)
  + MHRD
* Value added by our Project
  + It would be beneficial for 1st and 2nd year engineering students as Data Structures and Algorithms are usually thought in these years of study.
  + Highly beneficial for tier 2 and tier 3 college students who can
  + Use this to learn and understand the concept of infix-to-postfix conversion and postfix evaluation.
* Risks and Challenges
  + Works on python 2.7 may or may not work on python3
    
* Project Closure 
  + [[./docs/userfeedback][user-feedback]]
  + [[./docs/learnings][learnings]]
  + [[./docs/experience][experience]]
    
